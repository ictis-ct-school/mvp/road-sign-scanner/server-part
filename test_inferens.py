from ultralytics import YOLO
import cv2


data = cv2.imread('testing.jpg')

model = YOLO('best.pt')

res = model(data)

boxes = res[0].boxes.xyxy

print(data.shape)
for box in boxes:
    box = [int(cord) for cord in box] 

    cv2.rectangle(data, (box[1],box[3]), (box[0],box[2]), (222,222,0), 0)
    
cv2.imshow('frame', data)
cv2.waitKey(0)




