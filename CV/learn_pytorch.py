import torch
import matplotlib.pyplot as plt
import torch.optim as optim
import torchvision
from torchvision import datasets, transforms




# изучение основ раоты с тензорами 
a = torch.Tensor([1, 2])
b = torch.Tensor([5, 5])

rendom_tensor = torch.rand([5, 10])

rendom_tensor_view = rendom_tensor.view([10, 5])

print(rendom_tensor_view)
#Работа с датасетом 
transform_set = transforms.Compose([
        transforms.ToTensor()  ])

train = datasets.MNIST('data', train=True, download=True, transform=transform_set)

train_data = torch.utils.data.DataLoader(train, batch_size = 10, shuffle = True)

for l_data in train_data:
    
    test_element = l_data

    X_for_test, y_for_test = test_element[0][0], test_element[0][1]

    plt.imshow(X_for_test.view(28, 28))
    plt.show()

    print(y_for_test)
    break




class TestNetwork(torch.nn.Module):
    def __init__(self, *args):
        super().__init__()

        self.l1 = torch.nn.Linear(28*28, 10)
        self.l2 = torch.nn.Linear(10, 64)
        self.l3 = torch.nn.Linear(64, 10)
    
    def forward(self, x):
        x = torch.nn.functional.relu(self.l1(x))
        x = torch.nn.functional.relu(self.l2(x))
        x = torch.nn.functional.relu(self.l3(x))
        
        return torch.nn.functional.softmax(x, dim=1)

net = TestNetwork()

print(net)


# проверяем TestNetwork на случвйные данные 
test_img = torch.rand((28,28)).view(-1, 28*28)


output = net(test_img)
print(output)

#Реализация обучения 

loss_func = torch.nn.CrossEntropyLoss()
opmit = optim.Adam(net.parameters(), lr=0.001)

for ep in range(10):
    for batch_data in train_data:
        X, y = batch_data
        net.zero_grad()

        output = net(X.view(-1, 28*28))

        loss = torch.nn.functional.nll_loss(output, y)
        loss.backward()

        opmit.step()    

    print(loss)


#testing
log = output(X_for_test.view(-1, 28*28))


print('ожидалось:',y_for_test )
print('получиолсь:',log )