# Other imports
from ultralytics import YOLO
import cv2

# Inner imports
from common.pereferences import MODEL_PATH

class Detector:
    def __init__(self, path_to_weights = MODEL_PATH):
        self.model = YOLO(path_to_weights)

    def __str__(self):
        self.model.info(verbose=True)
        return f'Классы обученной модели: {self.model.names}\ndevice: {self.model.device}'

    def detect(self, image):
        if type(image) is str:
            image = cv2.imread(image)

        res = self.model(image[:,:,::-1])
        print(res)
        boxes = res[0].boxes.xyxy

        for box in boxes:
            box = [int(cord) for cord in box] 
            cv2.rectangle(image, (box[1],box[3]), (box[0],box[2]), (222,222,0), 0)  

        return image

    def detect_images(self, images):
        assert type(images) is not list, 'Use detect method for only image...'

        return [self.detect(image) for image in images]


if __name__ =="__main__":
    model = Detector()


    print(model)
    # resalt = model.detect('test.jpeg')
    # cv2.imshow('res', resalt)
    # cv2.waitKey(0)