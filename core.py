from flask import Flask

from common.pereferences import HOST

app = Flask(__name__)

import routes

app.run(debug=True, host = HOST)