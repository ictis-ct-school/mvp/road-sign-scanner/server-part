from ultralytics import YOLO

model = YOLO()

model.train(model = 'yolov8n.pt', data = 'config.yaml', dropout = 0.2, epochs = 30, lr0 = 0.05, verbose = True, optimizer = 'Adam', workers = 12)

model.export()